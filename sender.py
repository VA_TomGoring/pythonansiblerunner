import paho.mqtt.client as mqtt
import json

client = mqtt.Client()
client.connect("172.23.237.9", 1883, 60)

data = {
        "command": "start"
}

j = json.dumps(data)

client.publish("emulator_control", payload=j)