from json.decoder import JSONDecodeError
import paho.mqtt.client as mqtt
import ansible_runner
import json
import os
import shutil
import subprocess
import sys
import signal

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("emulator_control")

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))
    data = json.loads(msg.payload)
    if ('software_version' in data):
        try:
            r = ansible_runner.run(private_data_dir=".", playbook="./software_load/site.yml", inventory="./software_load/staging", extravars=data, limit="stb_lite_devices")
            if os.path.exists("artifacts"):
                shutil.rmtree("artifacts")
            if os.path.exists("env"):
                shutil.rmtree("env")
            client.publish("emulator_control", json.dumps({"status": r.status}))
        except Exception as e:
            print(e, file=sys.stderr)

    elif ('command' in data):
        if (data['command'] == "start"):
            print("Starting emulators")
            r = ansible_runner.run(private_data_dir=".", playbook="./start.yml", inventory="./software_load/staging", limit="stb_lite_devices")
        elif(data['command'] == "stop"):
            print("Stopping emulators")
            r = ansible_runner.run(private_data_dir=".", playbook="./stop.yml", inventory="./software_load/staging", limit="stb_lite_devices")
        elif(data['command'] == "restart"):
            print("Restarting emulators")
            r = ansible_runner.run(private_data_dir=".", playbook="./restart.yml", inventory="./software_load/staging", limit="stb_lite_devices")

def main():
    if (len(sys.argv) != 2):
        print("No broker IP given, exiting...")
        exit()

    try:
        shutil.rmtree("software_load")
    except:
        pass
    subprocess.check_call(["git", "clone", "git@bitbucket.org:vertaero/software_load.git"])
    subprocess.check_call(["git", "-C", "software_load", "checkout", "feature/add-stb-lite-group"])

    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    def signal_handler(sig, frame):
        client.disconnect()
        sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)

    client.connect(sys.argv[1], 1883, 60)
    client.subscribe("emulator_control")
    client.loop_forever()

main()